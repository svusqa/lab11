(define true (lambda (a b) a))
(define false (lambda (a b) b))
(define IsZero (lambda (n) (n (lambda (x) (false)) true)))
(define add(lambda (m n s z)(m s (n s z))))
(define pred (lambda (n) (n (lambda (g k) (g 1) (lambda (u) (add (g k) 1 ) k)) (lambda (v) 0) 0)))
(define sub(lambda(m n)(m pred n)))
(define Y (lambda (f) ((lambda (x) (f x x)) (lambda (x) (f x x)))))
(define LEQ (lambda (m n)(IsZero (sub m n))))
(define AND (lambda(m n a b)(n (m a b) b)))
(define EQ (lambda (m n) (AND (LEQ ( m n)) (LEQ (n m))))) 
(define L (lambda (n m) ((AND (NOT (EQ n m)) (LEQ n m) )))))
(define forCal (lambda (f n) (EQ (n 0)) 0 (add (f n-1)) ) )

(define uselessFunc(lambda (m n)(OR (IsZero (m))(L (m n)))(Y forCal(forCal n))(add m n) ))
	
	
	
